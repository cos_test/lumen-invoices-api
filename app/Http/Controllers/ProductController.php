<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    use ApiResponse;

    /**
     * Return invoices list
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $invoices = Product::all();
        return $this->successResponse($invoices);
    }

    /**
     * Create an instance of Invoice
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $products = Product::create($request->all());
        return $this->successResponse($products, Response::HTTP_CREATED);
    }

    /**
     * Return an specific invoice
     * @param $invoice
     * @return JsonResponse
     */
    public function show($invoice): JsonResponse
    {
        $invoice = Invoice::findOrFail($invoice);
        $invoice_products = $invoice->products;
        $invoice_customer = $invoice->customer;

        return $this->successResponse($invoice);
    }

    /**
     * Update thej information of an existing invoice
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $invoice): JsonResponse
    {
        $rules = [
            'name' => 'alpha|required|min:3|max:15',
            'last_name' => 'alpha|min:3|max:15',
            'phone_number' => 'numeric|digits_between:0,10',
            'document_number' => 'required|digits_between:0,12',
            'date_of_birth'=>'date|after:1922-01-01|before:2005-12-31'
        ];

        $this->validate($request, $rules);
        $invoice = Invoice::findOrFail($invoice);
        $invoice->fill($request->all());

        if ($invoice->isClean()) {
            return $this->errorResponse('At least one value must change', ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }

        $invoice->save();
        return $this->successResponse($invoice);
    }

    /**
     * Removes an existing invoice
     * @param $invoice
     * @return JsonResponse
     */
    public function destroy($invoice): JsonResponse
    {
        $invoice = Invoice::findOrFail($invoice);
        $invoice->delete();
        return $this->successResponse($invoice);
    }
}
