<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class CustomerController extends Controller
{
    use ApiResponse;

    /**
     * Return customers list
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $customers = Customer::all();
        return $this->successResponse($customers);
    }

    /**
     * Create an instance of Customer
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function store(Request $request): JsonResponse
    {
        $rules = [
            'name' => 'alpha|required|min:3|max:15',
            'last_name' => 'alpha|min:3|max:15',
            'phone_number' => 'numeric|digits_between:0,10',
            'document_number' => 'required|digits_between:0,12',
            'date_of_birth'=>'date|after:1922-01-01|before:2005-12-31'
        ];

        $this->validate($request, $rules);
        $customer = Customer::create($request->all());
        return $this->successResponse($customer, ResponseAlias::HTTP_CREATED);
    }

    /**
     * Return an specific customer
     * @param $customer
     * @return JsonResponse
     */
    public function show($customer): JsonResponse
    {
        $customer = Customer::findOrFail($customer);
        return $this->successResponse($customer);
    }

    /**
     * Update thej information of an existing customer
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request, $customer): JsonResponse
    {
        $rules = [
            'name' => 'alpha|required|min:3|max:15',
            'last_name' => 'alpha|min:3|max:15',
            'phone_number' => 'numeric|digits_between:0,10',
            'document_number' => 'required|digits_between:0,12',
            'date_of_birth'=>'date|after:1922-01-01|before:2005-12-31'
        ];

        $this->validate($request, $rules);
        $customer = Customer::findOrFail($customer);
        $customer->fill($request->all());

        if ($customer->isClean()) {
            return $this->errorResponse('At least one value must change', ResponseAlias::HTTP_UNPROCESSABLE_ENTITY);
        }

        $customer->save();
        return $this->successResponse($customer);
    }

    /**
     * Removes an existing customer
     * @param $customer
     * @return JsonResponse
     */
    public function destroy($customer): JsonResponse
    {
        $customer = Customer::findOrFail($customer);
        $customer->delete();
        return $this->successResponse($customer);
    }
}
