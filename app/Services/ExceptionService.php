<?php

namespace App\Services;

use App\Traits\ConsumesExternalService;

class ExceptionService
{
    use ConsumesExternalService;

    /**
     * The base uri to be used to consume the authors service
     * @var string
     */
    public $baseUri;

    public function __construct()
    {
        $this->baseUri = config('services.exceptions.base_uri');
    }

    public function setException($data)
    {
        return $this->performRequest('POST', '/exceptions', $data);
    }


}
