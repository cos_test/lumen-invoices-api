<?php

namespace App\Traits;

use App\Services\ExceptionService;
use Illuminate\Http\Response;

trait ApiResponse
{
    public $exceptionService;

    public function __construct(ExceptionService $exceptionService){
        $this->exceptionService = $exceptionService;
    }

    /**
     * Build a success response
     * @param  string|array $data
     * @param  int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function successResponse($data, $code = Response::HTTP_OK)
    {
        return response()->json(['data' => $data], $code);
    }

    /**
     * Build error responses
     * @param  string $message
     * @param  int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($message, $code)
    {
        $this->exceptionService->setException(['error' => json_encode($message), 'code' => $code]);
        return response()->json(['error' => $message, 'code' => $code], $code);
    }
}
