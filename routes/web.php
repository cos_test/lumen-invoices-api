<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/customers', 'CustomerController@index');
$router->post('/customers', 'CustomerController@store');
$router->get('/customers/{customer}', 'CustomerController@show');
$router->put('/customers/{customer}', 'CustomerController@update');
$router->patch('/customers/{customer}', 'CustomerController@update');
$router->delete('/customers/{customer}', 'CustomerController@destroy');

$router->get('/products', 'ProductController@index');
$router->post('/products', 'ProductController@store');
$router->get('/products/{product}', 'ProductController@show');
$router->put('/products/{product}', 'ProductController@update');
$router->patch('/products/{product}', 'ProductController@update');
$router->delete('/products/{product}', 'ProductController@destroy');

$router->get('/invoices', 'InvoiceController@index');
$router->post('/invoices', 'InvoiceController@store');
$router->get('/invoices/{invoice}', 'InvoiceController@show');
$router->put('/invoices/{invoice}', 'InvoiceController@update');
$router->patch('/invoices/{invoice}', 'InvoiceController@update');
$router->delete('/invoices/{invoice}', 'InvoiceController@destroy');
