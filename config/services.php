<?php

return [
    'exceptions' => [
        'base_uri' => env('EXCEPTIONS_SERVICE_BASE_URL'),
    ],
];

