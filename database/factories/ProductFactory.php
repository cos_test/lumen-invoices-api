<?php

namespace Database\Factories;

use App\Models\Product;

class ProductFactory extends \Illuminate\Database\Eloquent\Factories\Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            'name'=> $this->faker->name,
            'value'=> $this->faker->randomNumber(9),
        ];
    }
}
