<?php

namespace Database\Factories;

use App\Models\Customer;

class CustomerFactory extends \Illuminate\Database\Eloquent\Factories\Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            'name'=> $this->faker->name,
            'last_name' => $this->faker->lastName,
            'phone_number'=> '3205007010',
            'document_number'=> '10000400900',
            'date_of_birth'=> $this->faker->dateTimeInInterval('-100 years','-17 years'),
        ];
    }
}
