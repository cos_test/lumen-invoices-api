<?php

namespace Database\Factories;

use App\Models\Invoice;

class InvoiceFactory extends \Illuminate\Database\Eloquent\Factories\Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * @inheritDoc
     */
    public function definition(): array
    {
        return [
            'customer_id'=> $this->faker->randomNumber(9),
            'number' => $this->faker->lastName,
            'value'=> $this->faker->randomNumber(9),
            'company'=> $this->faker->name,
        ];
    }
}
